FROM node:9.11.1-alpine

# Install http-server to be able to run the http service to serve
# the application
RUN npm install -g http-server

# Create a separate directory named "application" that we can
# run the app from
WORKDIR /application

# We need to copy the package.json (and/or package-lock.json)
# to be able to install npm packages needed for this app
COPY package*.json ./

# Having copied the package.json file, download this 
# app's dependencies
RUN npm install

# Once we are sure that the requirements are downloaded successfully,
# copy the rest of the application
COPY . .

# Build the production version of the application
RUN npm run build


# Expose a port to be visible from the outside world, and run
# the application (which will automatically run on 8080)
EXPOSE 8080 
CMD ["http-server", "dist"]
